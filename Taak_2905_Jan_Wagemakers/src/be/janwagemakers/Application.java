/*
 ___________________________________________________________________________
/                                                                           \
| Taak Programmeren 3 : database FABRIEK - Afdeling - Machines - Onderdelen |
| Jan Wagemakers 2017                                                       |
\___  ______________________________________________________________________/
    |/
 (o_
 //\
 V_/_

 */
package be.janwagemakers;

public class Application {

	public Application() {
		GUI gui = new GUI();
		gui.setVisible(true);
		gui.showAll();
	}

	public static void main(String[] args) {
		new Application();
	}
}
