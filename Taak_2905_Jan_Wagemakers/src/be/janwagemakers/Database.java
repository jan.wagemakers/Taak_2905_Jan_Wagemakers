package be.janwagemakers;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import javax.swing.JOptionPane;

public class Database {
	private final static int COLUMNS = 6;	// Aantal kolommen dat we uit de database ophalen.
	
	// Hieronder de gegevens om met de database te verbinden.
	private String url="jdbc:mysql://localhost:3306/";
	private String user="root";
	private String pass="";					// Ivm. problemen root login mariadb Debian GNU/Linux, zie <https://askubuntu.com/questions/766334/cant-login-as-mysql-user-root-from-normal-user-account-in-ubuntu-16-04>
	private String database="FABRIEK";
		
	// mysql : statement, preparedStatement, connection 
	private Connection connection;
	private PreparedStatement preparedStatement;
	private Statement statement;
	private ResultSet resultSet;
	private String[][] result;
	private String[] values;
	public Database() {
		
	}
	
	/*
	 * Return : alles uit de database
	 */
	public String[][] getAll() {
		// System.out.println("Database.getAll");
		return search(makeQuery());
	}
	
	/* 
	 * Return : alle onderdelen waar in de omschrijving een bepaald woord voorkomt 
	 */
	public String[][] searchOnderdelenOmschrijving(String omschrijving) {
		String query = makeQuery("WHERE onderdelen.Omschrijving like ?");
		return search(query, "%" + omschrijving + "%");
	}
	
	/* 
	 * Return : alle machines waar in de omschrijving een bepaald woord voorkomt 
	 */
	public String[][] searchMachineOmschrijving(String omschrijving) {
		String query = makeQuery("WHERE Machines.Omschrijving like ?");
		return search(query, "%" + omschrijving + "%");
	}
	
	/*
	 * Return : alle machines die een bepaalde OHNR bevatten.
	 */
	public String[][] searchOnderdelenOHNR(String ohnr) {
		String query = makeQuery("WHERE onderdelen.OHNR=?");
		return search(query, ohnr);
	}
	
	/*
	 * Return : alle onderdelen die in een bepaalde Machine gebruikt worden. 
	 */
	public String[][] searchMachineNr(String mID) {
		String query = makeQuery("WHERE Machines.MID=?");
		return search(query, mID);
	}
	
	private String[][] search(String query, String search) {
		try{
			connect();
			preparedStatement = connection.prepareStatement(query);
			if (!search.equals("")) {
				preparedStatement.setString(1, search);
				preparedStatement.setString(2, search);
			}
			resultSet = preparedStatement.executeQuery();
			
			// temp store data in an ArrayList<String[]> because we don't know the size yet		
			ArrayList<String[]> temp = new ArrayList<>();
			while(resultSet.next())	{
				String[] row = new String[COLUMNS];
				for(int c = 1;c<=COLUMNS;c++) {
					row[c-1] = resultSet.getString("F" + c);
				}
				temp.add(row);
			}
			// convert ArrayList<String[]> to String[][]
			result = new String[temp.size()][COLUMNS];
			result = temp.toArray(result);
		} catch(Exception e) {
			error();
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				error();
			}
		}
		return result;
	}
	
	/*
	 * = search(query, "")
	 */
	private String[][] search(String query) {
		return search(query, "");
	}
		
	/*
	 * Aanmaken van Query voor Search
	 * 
	 * Via deze "superQuery" worden alle velden van de database samengevoegd en binnengehaald.
	 * In de software wordt dan via selectView bepaalde welke velden er zichtbaar worden.
	 * 
	 * Er wordt gebruik gemaakt van UNION omdat mySQL geen OUTER JOIN kent, daarom wordt een LEFT JOIN en een RIGHT JOIN samengevoegd.
	 */
	private String makeQuery(String string) {
		return "SELECT onderdelen.OHNR AS F1, onderdelen.Omschrijving AS F2, Machines.MID AS F3, Machines.OMSCHRIJVING AS F4, Afdelingen.AFID AS F5, Afdelingen.Omschrijving AS F6 " +
				   "FROM Afdelingen " +
				   "LEFT JOIN Machines ON Machines.AFID=Afdelingen.AFID " + 
				   "LEFT JOIN Machineonderdelen ON Machines.MID=Machineonderdelen.MID " + 
				   "LEFT JOIN onderdelen ON Machineonderdelen.OHNR=onderdelen.OHNR " +
				   string + " " +
				   "UNION " + 
				   "SELECT onderdelen.OHNR, onderdelen.Omschrijving, Machines.MID, Machines.OMSCHRIJVING, Afdelingen.AFID, Afdelingen.Omschrijving " +
				   "FROM Afdelingen " + 
				   "LEFT JOIN Machines ON Machines.AFID=Afdelingen.AFID " + 
				   "LEFT JOIN Machineonderdelen ON Machines.MID=Machineonderdelen.MID " + 
				   "RIGHT JOIN onderdelen ON Machineonderdelen.OHNR=onderdelen.OHNR " +
				   string + " " +
				   "ORDER BY F1, F3, F5";
	}
	
	/*
	 * = makeQuery("");
	 */
	private String makeQuery() {
		return makeQuery("");
	}
	
	/*
	 * Gemeenschappelijke code voor toevoegen/wissen/aanpassen
	 */
	private void exec(String uid) {
		try {
			connect();
			preparedStatement = connection.prepareStatement(uid);
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			error();
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				error();
			}
		}
	}
	
	/*
	 * Toevoegen van onderdeel
	 */
	public void addOnderdeel(String ohnr, String oOmschrijving) {
		String insert = "INSERT INTO `onderdelen` (`OHNR`, `OMSCHRIJVING`) VALUES ('" + ohnr + "', '" + oOmschrijving + "')";
		exec(insert);
	}
	
	/*
	 * Toevoegen van Machineonderdeel
	 */
	public void addMachineOnderdeel(String ohnr, String mID) {
		String insert = "INSERT INTO `Machineonderdelen` (`OHNR`, `MID`) VALUES ('" + ohnr + "', '" + mID + "')";
		exec(insert);
	}
	
	/*
	 * Toevoegen van Machine
	 */
	public void addMachine(String mID, String mOmschrijving, String afID) {
		String insert = "INSERT INTO `Machines` (`MID`, `OMSCHRIJVING`, `AFID`) VALUES ('" + mID + "', '" + mOmschrijving + "', '" + afID + "')";
		exec(insert);
	}
	
	/*
	 * Toevoegen van Afdeling
	 */
	public void addAfdeling(String afID, String mOmschrijving) {
		String insert = "INSERT INTO `Afdelingen` (`AFID`, `OMSCHRIJVING`) VALUES ('" + afID + "', '" + mOmschrijving + "')";
		exec(insert);
	}
	
	/*
	 * Verwijderen van Machines
	 */
	public void deleteMachine(String mID) {
		String delete = "DELETE FROM `Machines` WHERE MID='" + mID + "';";
		exec(delete);
	}
	
	/*
	 * Verwijderen van Onderdeel
	 */
	public void deleteOnderdeel(String ohNR) {
		String delete = "DELETE FROM `onderdelen` WHERE OHNR='" + ohNR + "';";
		exec(delete);
	}
	
	/*
	 * Verwijderen van MachineOnderdeel
	 */
	public void deleteMachineOnderdeel(String ohNR, String mID) {
		String delete = "DELETE FROM `Machineonderdelen` WHERE OHNR='" + ohNR + "' AND MID='" + mID + "';";
		exec(delete);
	}
	
	/*
	 * Verwijderen van Afdelingen
	 */
	public void deleteAfdeling(String afID) {
		String delete = "DELETE FROM `Afdelingen` WHERE AFID='" + afID + "';";
		exec(delete);
	}

	/*
	 * Updaten van een Afdeling
	 */
	public void updateAfdeling(String afID, String o) {
		String update = "UPDATE `Afdelingen` set OMSCHRIJVING='" + o + "'WHERE AFID='" + afID + "';";
		exec(update);
	}
	
	/*
	 * updaten van een onderdeel
	 */
	public void updateOnderdeel(String ohNR, String o) {
		String update = "UPDATE `onderdelen` set OMSCHRIJVING='" + o + "'WHERE OHNR='" + ohNR + "';";
		exec(update);
	}
	
	/*
	 * updaten van een Machine
	 */
	public void updateMachine(String mID, String o, String afID) {
		String update = "UPDATE `Machines` SET `OMSCHRIJVING`='" + o + "', `AFID`='" + afID + "' WHERE MID='" + mID + "';";
		exec(update);
	}
	
	/*
	 * Ophalen van alle onderdelen in een bepaalde machine
	 */
	public String[] getOnderdelen(String mID) {
		return get("SELECT OHNR AS X FROM "+ database + ".Machineonderdelen WHERE MID='" +mID+"'","OHNR");
	}
	
	/*
	 * Ophalen van alle onderdelen : SELECT OHNR FROM FABRIEK.onderdelen;
	 */
	public String[] getOnderdelen() {
		return get("SELECT OHNR AS X FROM "+ database + ".onderdelen", "OHNR");
	}
	
	/*
	 * Ophalen van alle onderdeelomschrijvingen
	 */
	public String[] getOOmschrijvingen() {
		return get("SELECT OMSCHRIJVING AS X FROM "+ database + ".onderdelen", "OHNR");
	}
	
	/*
	 * Ophalen van alle machines : SELECT MID FROM FABRIEK.Machines
	 */
	public String[] getMachines() {
		return	get("SELECT MID AS X FROM "+ database + ".Machines","MID");
	}
	
	/*
	 * Ophalen van de afdeling van een Machine
	 */
	public String getAfdeling(String mID) {
		return get("SELECT AFID AS X FROM "+ database + ".Machines WHERE MID='" + mID + "'","MID")[0];
	}
	
	/*
	 * Ophalen van de omschrijving van een Onderdeel
	 */
	public String getOOmschrijving(String ohNR) {
		return get("SELECT OMSCHRIJVING AS X FROM "+ database +".onderdelen WHERE OHNR='" + ohNR + "'","OHNR")[0];
	}
	
	/*
	 * Ophalen van alle machine met onderdelen
	 */
	public String[] getMachinesWithOnderdelen() {
		return	get("SELECT DISTINCT MID AS X FROM "+ database +".Machineonderdelen","MID");
	}
	
	/*
	 * Ophalen van alle machineomschrijvingen
	 */
	public String[] getMOmschrijvingen() {
		return	get("SELECT OMSCHRIJVING AS X FROM "+ database +".Machines","MID");
	}
	
	/*
	 * Ophalen van alle afdelingen : SELECT afID FROM FABRIEK.Afdelingen
	 */
	public String[] getAfdelingen() {
		return	get("SELECT afID AS X FROM "+ database +".Afdelingen","afID");
	}
	
	/*
	 * Ophalen van alle machines in een bepaalde afdeling
	 */
	public String[] getMachines(String afID) {
		return	get("SELECT MID AS X FROM "+ database +".Machines WHERE AFID=" + "'" + afID + "'","MID");
	}
	
	/*
	 * Ophalen van alle afdelingen : SELECT afID FROM FABRIEK.Afdelingen
	 */
	public String[] getAOmschrijvingen() {
		return	get("SELECT OMSCHRIJVING AS X FROM "+ database +".Afdelingen","afID");
	}
		
	/*
	 * Ophalen van X
	 */
	private String[] get(String query, String order){
		connect();
		query = query + " ORDER BY " + order;
		try{
			preparedStatement = connection.prepareStatement(query);
			resultSet = preparedStatement.executeQuery();
			
			ArrayList<String> temp = new ArrayList<>();
			while(resultSet.next())
			{
				temp.add(resultSet.getString("X"));
			}
			values = new String[temp.size()];
			values = temp.toArray(values);
			
		} catch(Exception e) {
			error();
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				error();
			}
		}
		return values;
	}
	
	
	/*
	 * Verbinding maken met de database.
	 */
	public void connect() {
		connect(false);
	}
	
	/*
	 * Verbinding maken met de database.
	 * 
	 * Als boolean testConnection = true -> test of verbinding kan gemaakt worden.
	 *                                      maak evt. een demo database aan.
	 * 
	 */
	public void connect(boolean testConnection) {
		try{
			Class.forName("com.mysql.jdbc.Driver").newInstance();
			connection = DriverManager.getConnection(url,user,pass);
					
			//create statement
			statement = connection.createStatement();
		}
			catch(Exception e){
			error();
		}
		try{
			//use database
			//statement.execute("DROP DATABASE "+ database);
			statement.execute("USE " + database);
		}
		catch(Exception e){
			if(testConnection) demo();
		} finally {
			if(testConnection) {
				try {
					connection.close();
				} catch (SQLException e) {
					error();
				}
			}
		}
	}
	
	/*
	 * Oeps, DATABASE ERROR
	 */
	private void error() {
		JOptionPane.showMessageDialog(null, "Kan geen verbinding maken met SQL-server!"
				+ "\n\nNeem contact op met de helpdesk (Tel: 5150)\n", "DATABASE ERROR", JOptionPane.ERROR_MESSAGE, null);
		System.exit(1);
	}
	
	/*
	 * Aanmaken van een demo database.
	 */
	public void demo() {
		JOptionPane.showMessageDialog(null, "Database niet gevonden!"
				+ "\n\nDEMO database wordt aangemaakt!", "DEMO DATABASE", JOptionPane.PLAIN_MESSAGE, null);
		try{
		//create and select the database for use
		statement.execute("SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0"); 
		statement.execute("SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0"); 
		statement.execute("SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES'"); 
		
		// statement.execute("DROP SCHEMA IF EXISTS " + database);
		statement.execute("CREATE DATABASE IF NOT EXISTS " + database + " DEFAULT CHARACTER SET utf8");
		statement.execute("USE " + database);
		
		//create table Afdelingen
		statement.execute("DROP TABLE IF EXISTS `Afdelingen`"); 
		statement.execute("CREATE TABLE IF NOT EXISTS `Afdelingen` (" 
				+ "`AFID` VARCHAR(4) NOT NULL DEFAULT ''," 
				+ "`OMSCHRIJVING` VARCHAR(40) NOT NULL DEFAULT '',"
				+ "PRIMARY KEY (`AFID`)) "
				+ "ENGINE = InnoDB "
				+ "DEFAULT CHARACTER SET = utf8");
		
		//create table Machines
		statement.execute("DROP TABLE IF EXISTS `Machines`"); 
		statement.execute("CREATE TABLE IF NOT EXISTS `Machines` (" 
				+ "`MID` VARCHAR(4) NOT NULL DEFAULT''," 
				+ "`OMSCHRIJVING` VARCHAR(40) NOT NULL DEFAULT ''," 
				+ "`AFID` VARCHAR(4) NOT NULL DEFAULT ''," 
				+ "PRIMARY KEY (`MID`)) " 
				+ "ENGINE = InnoDB " 
				+ "DEFAULT CHARACTER SET = utf8"); 

		//create table onderdelen
		statement.execute("DROP TABLE IF EXISTS `onderdelen`"); 
		statement.execute("CREATE TABLE IF NOT EXISTS `onderdelen` (" 
				+ "`OHNR` VARCHAR(13) NOT NULL DEFAULT ''," 
				+ "`OMSCHRIJVING` VARCHAR(100) NOT NULL DEFAULT ''," 
				+ "PRIMARY KEY (`OHNR`)) "
				+ "ENGINE = InnoDB " 
				+ "DEFAULT CHARACTER SET = utf8"); 
		
		//create table machineonderdelen
		statement.execute("DROP TABLE IF EXISTS `Machineonderdelen`"); 
		statement.execute("CREATE TABLE IF NOT EXISTS `Machineonderdelen` (" 
				+ "`OHNR` VARCHAR(13) NOT NULL DEFAULT ''," 
				+ "`MID` VARCHAR(4) NOT NULL DEFAULT ''," 
				+ "PRIMARY KEY (`OHNR` , `MID`)) "
				+ "ENGINE = InnoDB " 
				+ "DEFAULT CHARACTER SET = utf8");
		
		//ADDING FOREIGN KEYS
		statement.execute("ALTER TABLE `Machines` ADD CONSTRAINT `FK_AFID` FOREIGN KEY "
				+ "(`AFID`) REFERENCES `Afdelingen` (`AFID`) ON DELETE CASCADE ON UPDATE CASCADE");
		statement.execute("ALTER TABLE `Machineonderdelen` ADD CONSTRAINT `FK_OHNR` FOREIGN KEY "
				+ "(`OHNR`) REFERENCES `onderdelen` (`OHNR`) ON DELETE CASCADE ON UPDATE CASCADE");
		statement.execute("ALTER TABLE `Machineonderdelen` ADD CONSTRAINT `FK_MID` FOREIGN KEY "
				+ "(`MID`) REFERENCES `Machines` (`MID`) ON DELETE CASCADE ON UPDATE CASCADE");
		
		statement.execute("SET SQL_MODE=@OLD_SQL_MODE"); 
		statement.execute("SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS"); 
		statement.execute("SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS");
		
		addAfdeling("OVEN", "Thermische Behandeling");
		addAfdeling("RT", "Rondsels & Tandwielen");
		addAfdeling("CD", "Carters & Deksels");
		addAfdeling("MONT", "Montage");
		addAfdeling("OHD", "Onderhoud");
		addAfdeling("RAD", "Services");
		
		addMachine("T005","SMIDSE","OHD");
		addMachine("T013","AICHELIN Ontlaatoven","OVEN");
		addMachine("T014","AICHELIN Oven","OVEN");
		addMachine("T015","AICHELIN Oven","OVEN");
		addMachine("T017","IVA Karboneeroven","OVEN");
		addMachine("T019","IVA Ontlaatoven","OVEN");
		addMachine("T020","HERING Oliebad","OVEN");
		addMachine("T021","AICHELIN Carboneeroven (Ombouw Hansen)","OVEN");
		addMachine("T022","IVA Oven","OVEN");
		addMachine("T024","IVA Oven","OVEN");
		addMachine("T025","OLIEBAD","OVEN");
		addMachine("T026","IVA Oven","OVEN");
		addMachine("T027","IVA Oven","OVEN");
		addMachine("T029","SERTT Endogasgenerator","OVEN");
		addMachine("T030","Ventilator rookafzuiging","OVEN");
		addMachine("T031","IVA Oven","OVEN");
		addMachine("T032","Cieffe Forni oliebad","OVEN");
		addMachine("T050","Oven FORNAX","MONT");
		addMachine("T051","Oven FORNAX","MONT");
		addMachine("T052","Oven FORNAX","OVEN");
		addMachine("T100","WISCON Supervisiesysteem","OVEN");
		addMachine("S019","FROMAG (Amber)","RAD");
		addMachine("S020","FROMAG","RT");
		addMachine("S021","FROMAG","RT");
		addMachine("S022","FROMAG","CD");
		addMachine("S024","FROMAG","CD");
		addMachine("S025","ALEXY","CD");
		addMachine("S026","FROMAG RAPIDA","RT");
		addMachine("S027","FROMAG","RT");
		addMachine("S064","RUS Steekmachine","CD");
		addMachine("R002","CINCINATTI","RAD");
		addMachine("R010","SCHAUDT","RT");
		addMachine("R011","STUDER","RT");
		addMachine("R013","KARSTENS","RT");
		addMachine("R014","KARSTENS NIEUW","RT");
		addMachine("R020","GLAUCHAU (1986)","RT");
		addMachine("R024","GLAUCHAU","RT");
		addMachine("R025","GLAUCHAU","RT");
		addMachine("R026","KEHREN (I202)","RT");
		addMachine("R044","REISHAUER (Gereviseerde)","RT");
		addMachine("R050","PFAUTER-KAPP","RT");
		addMachine("R051","GLEASON PFAUTER","RT");
		addMachine("R052","GLEASON PFAUTER","RT");
		addMachine("R053","REISHAUER (I109)","RT");
		addMachine("R054","GLEASON PFAUTER (Amber)","RAD");
		addMachine("R055","GLEASON PFAUTER","RAD");
		addMachine("R058","REISHAUER (I113)","RT");
		addMachine("R059","REISHAUER (I112)","RT");
		addMachine("R061","REISHAUER ZB (I108)","RT");
		addMachine("R062","REISHAUER","RT");
		addMachine("R063","PFAUTER KAPP","RT");
		addMachine("R064","OERLIKON MAAG","RT");
		addMachine("R065","OERLIKON-MAAG","RT");
		addMachine("R066","PFAUTER KAPP","RT");
		addMachine("R067","OERLIKON-MAAG","RT");
		addMachine("R068","GLAESON PFAUTER","RT");
		addMachine("R069","GLAESON PFAUTER","RT");
		addMachine("R072","DISCUS","CD");
		addMachine("R073","DISCUS","MONT");
		addMachine("R074","ELB","MONT");
		addMachine("R075","ELB","MONT");
		addMachine("R076","DELTA","RAD");
		addMachine("R077","DELTA","MONT");
		addMachine("R078","TOS","CD");
		addMachine("R079","ELB","MONT");
		addMachine("R080","DAVIS THOMPSON (Rotomatic) (I246)","RT");
		addMachine("R081","PICCO (I241)","RT");
		addMachine("R082","PICCO (I238)","RT");
		addMachine("R083","GRATOMAT ontbraammachine","RT");
		addMachine("R084","BOSCH","RT");
		addMachine("R085","GRATOMAT wordt vervangen door R088","RT");
		addMachine("R086","GRATOMAT RAUSCH","RT");
		addMachine("R087","PICCO (Amber) (I218)","RAD");
		addMachine("R088","PICCO (I225)","RT");
		addMachine("R089","PICCO (I239)","RT");
		addMachine("R090","ELB","MONT");
		addMachine("R091","PICCO","RT");
		addMachine("R093","HENNIGER Centreerslijpmachine","OVEN");
		addMachine("R094","Centreerslijpmachine+DraaibankS","OVEN");
		addMachine("R095","HENNINGER","OVEN");
		addMachine("R102","REISHAUER Filter","RT");
		addMachine("A068","FreesMachine KELVIN DUTCH", "CD");
		addMachine("V001","Gele transportwagen VITAMITJE", "OVEN");

		addOnderdeel("OH-0000010735", "Regelbare klemhandel met bout Halder HWN441-G25-M12x40oranje");
		addOnderdeel("OH-0000033573", "PM-Regelaar Z-as - nr.4197711-0010 - Emag nr.4534806");
		addOnderdeel("OH-0000033574", "PM-Regelaar Z-as - nr.4199055-0010 - Emag nr.4534632");
		addOnderdeel("OH-0000014258", "Thyristorsturing thyrotact A2 160A");
		addOnderdeel("OH-0000032456", "Brandersturing Rekumat A800D14 55");
		addOnderdeel("OH-0000024458", "Eurotherm regelaar 3504");
		addOnderdeel("OH-0000011111", "Eurotherm regelaar 902S");
		addOnderdeel("OH-0000011112", "Eurotherm regelaar 903P");
		addOnderdeel("OH-0000013926", "Behuizing (inox buis 1 -26,7mm - L=900mm)  vr meetsonde CS85  CS87");
		addOnderdeel("OH-0000013929", "Debietmeter voor aardgas max.550l/h nr.218167");
		addOnderdeel("OH-0000015184", "Kroonwiel M1212-14 voor handbediening langsslede");
		addOnderdeel("OH-0000015185", "Rondselas M1212-1");
		addOnderdeel("OH-0000015186", "Rondselas M1212-2");
		addOnderdeel("OH-0000015187", "Rondselas M1212-11");
		addOnderdeel("OH-0000015188", "Rondselas M1212-13 voor handbeweging langsslede");
		addOnderdeel("OH-0000015861", "Koolborstelhouder (3-voudig) G2334/200 vr.motor Siemens type  1HA3207-5ZM40-Z");
		addOnderdeel("OH-0000019856", "Getande riem 500-5M-30 (voor X-as R011)");
		addOnderdeel("OH-0000020527", "Meeneempen G2244-270");
		addOnderdeel("OH-0000030702", "Meetlat Heidenhain type LS406 - lengte 320mm. - art.nr.310110-21");
		addOnderdeel("OH-0000030716", "Set dichtingen TC01 voor Brinkmann pomp TC40/340");
		addOnderdeel("OH-0000031688", "Vlotterschakelaar Bernstein type MAY-711 - BAS0135 - art.nr.6811362015 - voor smeereenheid Prelub");
		addOnderdeel("OH-0000031743", "Spoel 400VAC nr.1943588 - voor rem van motor SEW type DFY71");
		addOnderdeel("OH-0000033567", "Filterelement Mahle PI 9545 DRG VST1000 - 1000mu");
		addOnderdeel("OH-0000033569", "Beeldscherm Simatic - nr.6FC5103-0AB01-0AA2");
		addOnderdeel("OH-0000033839", "Inox buis diam.60x3x1200mm MET FLENS (W.ST.4841)");
		addOnderdeel("OH-0000034913", "Veiligheidsschakelaar Euchner TZ1RE024SR11 ID 070826");
		addOnderdeel("OH-0000022864", "Kabel diam.8mm. - 19x7 (verzinkt draaivrij)");
		addOnderdeel("OH-0000000806", "Getande riemschijf (staal) 24T - ST9,525 - BR25,4 (24L100)");
		addOnderdeel("OH-0000010940", "Spantang diam. 6mm. met moer M15x1 nr. 2.608.570.084");
		addOnderdeel("OH-0000016367", "Balg voor slijpkop (zeskant)");
		addOnderdeel("OH-0000016371", "Draadstang M1138-19 + moer M1138-20 voor riemtransmissie)");
		addOnderdeel("OH-0000016373", "Wormwiel M1132-5 (fiber)voor magneetrol");
		addOnderdeel("OH-0000016375", "Smeerpomp type D/6  voor omloopsmering");
		addOnderdeel("OH-0000032317", "Converter JK9000N1");	
		addOnderdeel("OH-0000032579", "Stenen pot (Inerte blok) voor kraakpot");
		addOnderdeel("OH-0000032794", "Rol filterpapier 100cmx100m INT.65");
		addOnderdeel("OH-0000012481", "Batterij Varta type 3/60DK (3,6V-60mA) met soldeerlippen");
		addOnderdeel("OH-0000031572", "Drukveer diam.5x2x25mm");
		addOnderdeel("OH-0000012850", "Naderingsschak. Balluff BES 516-325-S4-C (10-30VDC M12x1)");
		addOnderdeel("OH-0000018720", "Frequentieomvormer PVSF1-PT001001 regelbereik:15-54000 t/min");
		addOnderdeel("OH-0000026124", "Staande Y-blok RASE 75 FA 164.1 in gietijzer");
		addOnderdeel("OH-0000026309", "Barrierta L55/2 (patroon van 800gr.) -40...260°C");
		addOnderdeel("OH-0000031219", "SM Simatic S5 nr.6ES5454-7LA11 (digital output module)");
		addOnderdeel("OH-0000032142", "Klemblokje nr.KE1314 - breedte 45mm - voor meshouder WST09/600-1");
		addOnderdeel("OH-0000004021", "O-ring diam.110x5mm.");
		
		addMachineOnderdeel("OH-0000004021", "R058");
		addMachineOnderdeel("OH-0000004021", "R059");
		addMachineOnderdeel("OH-0000004021", "R069");
		addMachineOnderdeel("OH-0000004021", "R061");
		addMachineOnderdeel("OH-0000004021", "R062");
		addMachineOnderdeel("OH-0000004021", "R063");
		addMachineOnderdeel("OH-0000032142", "S026");
		addMachineOnderdeel("OH-0000032142", "S019");
		addMachineOnderdeel("OH-0000032142", "S020");
		addMachineOnderdeel("OH-0000032142", "S021");
		addMachineOnderdeel("OH-0000031219", "T022");
		addMachineOnderdeel("OH-0000031219", "T024");
		addMachineOnderdeel("OH-0000026309", "T050");
		addMachineOnderdeel("OH-0000026309", "T051");
		addMachineOnderdeel("OH-0000026309", "T052");
		addMachineOnderdeel("OH-0000026124", "T050");
		addMachineOnderdeel("OH-0000026124", "T051");
		addMachineOnderdeel("OH-0000026124", "T052");
		addMachineOnderdeel("OH-0000018720", "R094");
		addMachineOnderdeel("OH-0000012850", "R074");
		addMachineOnderdeel("OH-0000012850", "R065");
		addMachineOnderdeel("OH-0000012850", "R067");
		addMachineOnderdeel("OH-0000012850", "R064");
		addMachineOnderdeel("OH-0000012850", "R075");
		addMachineOnderdeel("OH-0000012850", "R014");
		addMachineOnderdeel("OH-0000012850", "R069");
		addMachineOnderdeel("OH-0000031572", "R072");
		addMachineOnderdeel("OH-0000031572", "R073");
		addMachineOnderdeel("OH-0000012481", "R044");
		addMachineOnderdeel("OH-0000012481", "R050");
		addMachineOnderdeel("OH-0000012481", "R051");
		addMachineOnderdeel("OH-0000012481", "R052");
		addMachineOnderdeel("OH-0000012481", "R053");
		addMachineOnderdeel("OH-0000012481", "R054");
		addMachineOnderdeel("OH-0000012481", "R055");
		addMachineOnderdeel("OH-0000010735", "R080");
		addMachineOnderdeel("OH-0000010735", "R081");
		addMachineOnderdeel("OH-0000010735", "R082");
		addMachineOnderdeel("OH-0000010735", "R083");
		addMachineOnderdeel("OH-0000010735", "R084");
		addMachineOnderdeel("OH-0000010735", "R085");
		addMachineOnderdeel("OH-0000010735", "R086");
		addMachineOnderdeel("OH-0000010735", "R087");
		addMachineOnderdeel("OH-0000010735", "R088");
		addMachineOnderdeel("OH-0000010735", "R089");
		addMachineOnderdeel("OH-0000010735", "R090");
		addMachineOnderdeel("OH-0000032794", "R026");
		addMachineOnderdeel("OH-0000032317", "T029");
		addMachineOnderdeel("OH-0000032579", "T029");
		addMachineOnderdeel("OH-0000016367", "R024");
		addMachineOnderdeel("OH-0000016371", "R024");
		addMachineOnderdeel("OH-0000016373", "R024");
		addMachineOnderdeel("OH-0000016375", "R024");
		addMachineOnderdeel("OH-0000016375", "R025");
		addMachineOnderdeel("OH-0000010940", "R082");
		addMachineOnderdeel("OH-0000010940", "R081");
		addMachineOnderdeel("OH-0000010940", "R087");
		addMachineOnderdeel("OH-0000010940", "R088");
		addMachineOnderdeel("OH-0000010940", "R085");
		addMachineOnderdeel("OH-0000000806", "R102");
		addMachineOnderdeel("OH-0000022864", "V001");
		addMachineOnderdeel("OH-0000034913", "R013");
		addMachineOnderdeel("OH-0000034913", "R014");
		addMachineOnderdeel("OH-0000033839", "T026");
		addMachineOnderdeel("OH-0000033839", "T022");
		addMachineOnderdeel("OH-0000033839", "T024");
		addMachineOnderdeel("OH-0000033839", "T015");
		addMachineOnderdeel("OH-0000033839", "T027");
		addMachineOnderdeel("OH-0000033573", "R013");
		addMachineOnderdeel("OH-0000033573", "R014");
		addMachineOnderdeel("OH-0000033574", "R013");
		addMachineOnderdeel("OH-0000033574", "R014");
		addMachineOnderdeel("OH-0000033569", "A068");
		addMachineOnderdeel("OH-0000033567", "R066");
		addMachineOnderdeel("OH-0000033567", "R065");
		addMachineOnderdeel("OH-0000033567", "R050");
		addMachineOnderdeel("OH-0000031743", "S021");
		addMachineOnderdeel("OH-0000031743", "S026");
		addMachineOnderdeel("OH-0000031743", "S027");
		addMachineOnderdeel("OH-0000031688", "R064");
		addMachineOnderdeel("OH-0000031688", "R065");
		addMachineOnderdeel("OH-0000031688", "R067");
		addMachineOnderdeel("OH-0000030716", "R050");
		addMachineOnderdeel("OH-0000030716", "R068");
		addMachineOnderdeel("OH-0000030716", "R066");
		addMachineOnderdeel("OH-0000030716", "R069");
		addMachineOnderdeel("OH-0000030716", "R052");
		addMachineOnderdeel("OH-0000030702", "R020");
		addMachineOnderdeel("OH-0000020527", "R013");
		addMachineOnderdeel("OH-0000020527", "R014");
		addMachineOnderdeel("OH-0000020527", "R010");
		addMachineOnderdeel("OH-0000019856", "R011");
		addMachineOnderdeel("OH-0000015861", "R010");
		addMachineOnderdeel("OH-0000011112", "T013");
		addMachineOnderdeel("OH-0000011112", "T019");
		addMachineOnderdeel("OH-0000011112", "T017");
		addMachineOnderdeel("OH-0000011111", "T017");
		addMachineOnderdeel("OH-0000011111", "T020");
		addMachineOnderdeel("OH-0000011112", "T021");
		addMachineOnderdeel("OH-0000011111", "T021");
		addMachineOnderdeel("OH-0000011111", "T025");
		addMachineOnderdeel("OH-0000011111", "T032");
		addMachineOnderdeel("OH-0000011112", "T022");
		addMachineOnderdeel("OH-0000011111", "T022");
		addMachineOnderdeel("OH-0000032456", "T026");
		addMachineOnderdeel("OH-0000024458", "T026");
		addMachineOnderdeel("OH-0000032456", "T027");
		addMachineOnderdeel("OH-0000011111", "T027");
		addMachineOnderdeel("OH-0000011112", "T027");
		addMachineOnderdeel("OH-0000032456", "T024");
		addMachineOnderdeel("OH-0000032456", "T022");
		addMachineOnderdeel("OH-0000013926", "T017");
		addMachineOnderdeel("OH-0000013926", "T014");
		addMachineOnderdeel("OH-0000013926", "T015");
		addMachineOnderdeel("OH-0000013926", "T021");
		
		addMachineOnderdeel("OH-0000014258", "T017");
		addMachineOnderdeel("OH-0000014258", "T021");
		addMachineOnderdeel("OH-0000014258", "T014");
		addMachineOnderdeel("OH-0000024458", "T014");
		addMachineOnderdeel("OH-0000014258", "T031");
		addMachineOnderdeel("OH-0000024458", "T031");
		
		addMachineOnderdeel("OH-0000013929", "T017");
		addMachineOnderdeel("OH-0000013929", "T015");
		addMachineOnderdeel("OH-0000013929", "T014");
		addMachineOnderdeel("OH-0000013929", "T031");
		addMachineOnderdeel("OH-0000013929", "T021");
		addMachineOnderdeel("OH-0000013929", "T026");
		addMachineOnderdeel("OH-0000013929", "T027");
		addMachineOnderdeel("OH-0000013929", "T024");
		addMachineOnderdeel("OH-0000013929", "T022");
		
		addMachineOnderdeel("OH-0000015184", "R002");
		addMachineOnderdeel("OH-0000015185", "R002");
		addMachineOnderdeel("OH-0000015186", "R002");
		addMachineOnderdeel("OH-0000015187", "R002");
		addMachineOnderdeel("OH-0000015188", "R002");
		}
		catch(Exception e){
			error();
		}
	}
}
