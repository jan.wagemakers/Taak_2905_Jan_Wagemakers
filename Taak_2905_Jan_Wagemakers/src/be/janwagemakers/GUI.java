/*
 * Deze klasse is verantwoordelijk voor het tonene van de GUI
 */
package be.janwagemakers;

import javax.swing.Box;
import javax.swing.DefaultListModel;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.border.EmptyBorder;
import javax.swing.event.AncestorEvent;
import javax.swing.event.AncestorListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyEvent;
import java.util.ArrayList;

@SuppressWarnings("serial")
public class GUI extends JFrame {

	private static final int 		LENAFID 	= 4;
	private static final int 		LENAFO 		= 40;
	private static final int 		LENMID 		= 4;
	private static final int 		LENMO 		= 40;
	private static final int 		LENOID 		= 13;
	private static final int 		LENOO 		= 100;
	private static final int 		INSERT 		= 1;
	private static final int 		UPDATE 		= 2;
	private static final int 		DELETE 		= 3;
	private static final int		SEARCHOO	= 4;
	private static final int		SEARCHMO	= 4;
	private static final int		SEARCHOHNR	= 5;
	private static final int		SEARCHMID	= 5;
	private static final String 	NOTFOUND 	= "Lijst is leeg!";
	private static final Dimension 	DIM 		= new Dimension(500, 300);
	// Menu Item gedefineerd als statische Strings
	private static final String 	MSEARCH 	= "Zoek";
	private static final int		MSEARCHKEY	= KeyEvent.VK_Z;
	private static final String 	MDELETE 	= "Verwijderen";
	private static final int	 	MDELETEKEY 	= KeyEvent.VK_V;
	private static final String 	MINSERT 	= "Toevoegen";
	private static final int	 	MINSERTKEY 	= KeyEvent.VK_T;
	private static final String 	MUPDATE 	= "Aanpassen";
	private static final int	 	MUPDATEKEY 	= KeyEvent.VK_A;
	private static final String 	MHELP   	= "Help";
	private static final int	 	MHELPKEY   	= KeyEvent.VK_H;
	private static final String 	MVIEW   	= "Zichtbaarheid";
	private static final int	 	MVIEWKEY   	= KeyEvent.VK_I;
	private static final String 	MMACH   	= "Machine";
	private static final String 	MPART   	= "Onderdeel";
	private static final String 	MMACHPART 	= "MachineOnderdeel";
	private static final String 	MDEP		= "Afdeling";
	private static final String 	MABOUT		= "Over";
	private static final String 	MALL		= "Alles";
	private static final String 	MOHNR		= "OH-nummmer";
	private static final String 	MDESCPART	= "Omschrijving Onderdeel";
	private static final String 	MMACHNR		= "Machine-nummer";
	private static final String 	MDESCMACH	= "Omschrijving Machine";
		
	private JPanel contentPane;
	private DefaultTableModel model;
	private JScrollPane scrollPane;
	private JTable table;
	private Data data;
	private Database database;
	private SelectView selectView;
	private JList<String> afIDL;
	private JScrollPane lAfID;
	private ArrayList<String> omschrijvingenAL;
	
	/*
	 * Start van aanmaken van de GUI	
	 */
	public GUI() {
		database = new Database();											// Data komt van database, dus
		database.connect(true);												// connecteer met de database
																			// test of de database bestaat
																			// maak evt. een demo database aan
																			// sluit de connectie terug
				
		selectView = new SelectView(this);									// selectView --> welke velden wil de gebruiker zien?
		data = new Data(selectView);										// data = data afkomstig uit de database
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setTitle("Machine-Onderdelen Overzicht");
		setBounds(100, 100, 1280, 768);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		// MenuBar van boven aan het scherm
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		//de menu's
		String[] str;
		str = new String[] {MALL, MOHNR, MDESCPART+"_O", MMACHNR, MDESCMACH+"_M"};
		JMenu zoek = new JMenu(MSEARCH);
		zoek.setMnemonic(MSEARCHKEY);
		menuBar.add(zoek);
		new SubMenus(this, zoek, str);

		str=new String[] {MDEP, MMACH+"_N", MPART+"_I", MMACHPART+"_P"};
		JMenu invoegen = new JMenu(MINSERT);
		invoegen.setMnemonic(MINSERTKEY);
		menuBar.add(invoegen);
		new SubMenus(this, invoegen, str);
		
		str=new String[] {MDEP, MMACH+"_B", MPART+"_U", MMACHPART+"_Q"};
		JMenu wissen = new JMenu(MDELETE);
		wissen.setMnemonic(MDELETEKEY);
		menuBar.add(wissen);
		new SubMenus(this, wissen, str);
		
		str=new String[] {MDEP, MMACH+"_G", MPART+"_H"};
		JMenu aanpassen = new JMenu(MUPDATE);
		aanpassen.setMnemonic(MUPDATEKEY);
		menuBar.add(aanpassen);
		new SubMenus(this, aanpassen, str);
				
		JMenu toon = new JMenu(MVIEW);
		toon.setMnemonic(MVIEWKEY);
		menuBar.add(toon);
		str = new String[selectView.getNames().length];						// Lijst van alle velden die geselecteerd kunnen worden
		for(int c=0;c<selectView.getNames().length;c++) {
			str[c] = selectView.getNames()[c] + "_" + (c+1);				// Aanmaken van de CTRL 1....6 accel_keys
		}
		new SubMenus(this, toon, str, true);								// Lijst van alle velden + accel_keys in submenu
		for(int c=0;c<selectView.getNames().length;c++) {
			((JMenuItem) toon.getMenuComponent(c)).setSelected(selectView.getView(c));
		}
		
		JMenu help = new JMenu(MHELP);
		help.setMnemonic(MHELPKEY);
		menuBar.add(Box.createHorizontalGlue());
		menuBar.add(help);
		str=new String[] {MABOUT};
		new SubMenus(this, help, str);
		
		// Om de data in de JTable te tonen werken we met een model
        model = new DefaultTableModel(0, selectView.countView()){

            @Override
            public boolean isCellEditable(int r, int c) {
                return false; 	// false -> cell kan niet aangepast worden.
            }

           };
        table = new JTable(model);			// toon de inhoud van het model in een JTable
          
        //add the table to the frame
        scrollPane = new JScrollPane(table);
        //scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
        contentPane.add(scrollPane);
	}

	/*
	 * Via de klasse submenu wordt er terug naar hier gesprongen als er op een menuItem is geklikt.
	 * 
	 * Via JMenuItem, achterhalen we het submenu en het menu waar is op geklikt.
	 */
	public void menu(JMenuItem menuItem) {
		String subMenuStr = menuItem.getText();					// Wat was het subMenu?
		JPopupMenu jpm = (JPopupMenu) menuItem.getParent();
		JMenu menu = (JMenu) jpm.getInvoker();					// Wat was het menu?
		String menuStr = menu.getText();
		// Voer op basis van menu en subMenu de gewenste methode uit.
		switch (menuStr) {
		case MHELP :
			switch (subMenuStr) {
			case MABOUT :
				JOptionPane.showMessageDialog(this, "<HTML><CENTER>2017 Jan Wagemakers<BR><BR>"
						+ ";-)<BR><BR></CENTER></HTML>");
				break;
			}
			break;
		case MSEARCH :
			switch (subMenuStr) {
			case MALL :
				showAll();
				break;
			case MOHNR :
				suidOnderdeel(SEARCHOHNR);
				break;
			case MDESCPART :
				suidOnderdeel(SEARCHOO);
				break;
			case MMACHNR :
				suidMachine(SEARCHMID);
				break;
			case MDESCMACH :
				suidMachine(SEARCHMO);
				break;
			}
			break;
		case MINSERT :
			switch (subMenuStr) {
			case MDEP :
				iudAfdeling(INSERT);
				break;
			case MMACH :
				suidMachine(INSERT);
				break;
			case MPART :
				suidOnderdeel(INSERT);
				break;
			case MMACHPART :
				idMachineOnderdeel(INSERT);
				break;
			}
			break;
		case MUPDATE :
			switch (subMenuStr) {
			case MDEP :
				iudAfdeling(UPDATE);
				break;
			case MMACH :
				suidMachine(UPDATE);
				break;
			case MPART :
				suidOnderdeel(UPDATE);
				break;
			}
			break;
		case MDELETE :
			switch (subMenuStr) {
			case MDEP :
				iudAfdeling(DELETE);
				break;
			case MMACH :
				suidMachine(DELETE);
				break;
			case MPART :
				suidOnderdeel(DELETE);
				break;
			case MMACHPART :
				idMachineOnderdeel(DELETE);
				break;
			}
			break;
		case MVIEW :
			selectView.toggleView(subMenuStr);				      		// toggle de zichtbaarheid van het geselecteerde veld.
			if (selectView.countView() == 0) menuItem.doClick();		// Minstens 1 kolom moet zichtbaar blijven
			break;
		}
	}
		
	/*
	 * InsertDelete MachineOnderdeel
	 * 
	 * id = 1 : Insert
	 * id = 3 : Delete
	 */
	private void idMachineOnderdeel(int id) {
		DefaultListModel<String> model = new DefaultListModel<String>();
		model.setSize(0);
		
		String[] machines = null;
		String titleStr = "";
		if (id == DELETE) {
			machines = database.getMachinesWithOnderdelen();
			titleStr = "Een Onderdeel uit een Machine verwijderen";
		}
		if (id == INSERT) {
			machines = database.getMachines();
			titleStr = "Een Onderdeel aan een Machine toevoegen";
		}
		
		JList<String> mID = new JList<String>(machines);
		focus(mID);
		JList<String> ohNR = new JList<String>(model);
		String[] ms = database.getMOmschrijvingen();
		JLabel mO = new JLabel("");
		mO.setForeground(Color.BLUE);
		JLabel oO = new JLabel("");
		oO.setForeground(Color.BLUE);
		
		ohNR.addListSelectionListener(new ListSelectionListener(){

			@Override
			public void valueChanged(ListSelectionEvent arg0) {
				if(ohNR.getSelectedValue() != null)	oO.setText(database.getOOmschrijving(ohNR.getSelectedValue()));
			}
		});
		
		mID.addListSelectionListener(new ListSelectionListener(){

			@Override
			public void valueChanged(ListSelectionEvent arg0) {
				//System.out.println(ohNR.getSelectedValue());
				mO.setText(ms[mID.getSelectedIndex()]);
				model.setSize(0);
				String[] onderdelen = null;
				String[] myOnderdelen = null;
				// if DELETE --> lijst met onderdelen die tot deze machine behoren
				// if INSERT --> lijst met onderdelen die NIET tot deze machine behoren
				if (id == DELETE) onderdelen = database.getOnderdelen(mID.getSelectedValue());
				if (id == INSERT) {
					onderdelen = database.getOnderdelen();
					myOnderdelen = database.getOnderdelen(mID.getSelectedValue());
				}
				for(String onderdeel : onderdelen ) {
					boolean own=false;
					if (id == INSERT) {
						for(String myOnderdeel : myOnderdelen) {
							if(onderdeel.equals(myOnderdeel)) own=true; 
						}
					}
					if(!own) model.addElement(onderdeel);
				}		
				ohNR.setSelectedIndex(0);
				ohNR.ensureIndexIsVisible(0);
			}
		});
		
		mID.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		ohNR.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		mID.setSelectedIndex(0);
		ohNR.setSelectedIndex(0);
		JScrollPane lMID = new JScrollPane(mID);
		lMID.setPreferredSize(DIM);
		JScrollPane lOhNR = new JScrollPane(ohNR);
		lOhNR.setPreferredSize(DIM);
		int result = JOptionPane.showConfirmDialog(null, new Object[] {
				"Machine-nummer", lMID,
				mO,
				"OH-nummer", lOhNR,
				oO
		}, titleStr, JOptionPane.OK_CANCEL_OPTION);
		if (result == JOptionPane.OK_OPTION) {
			if (ohNR.getSelectedValue().isEmpty()) return;
			if (mID.getSelectedValue().isEmpty()) return;
			if (id == INSERT) database.addMachineOnderdeel((String) ohNR.getSelectedValue(), (String) mID.getSelectedValue());
			if (id == DELETE) database.deleteMachineOnderdeel((String) ohNR.getSelectedValue(), (String) mID.getSelectedValue());
			showAll();
		}
	}
	
	/*
	 * Gemeenschappelijke code voor het tonen/selecteren van een afdeling
	 */
	private void guiAfdeling(int d) {
		String[] afdelingenAR = database.getAfdelingen();
		String[] omschrijvingenAR = database.getAOmschrijvingen();
		omschrijvingenAL = new ArrayList<>();
		ArrayList<String> afdelingenAL = new ArrayList<>();
		for(int c=0;c<afdelingenAR.length;c++) {
			// Ook niet lege afdelingen in lijst opnemen?
			if((database.getMachines(afdelingenAR[c]).length == 0) || (d != DELETE)) {
				afdelingenAL.add(afdelingenAR[c]);
				omschrijvingenAL.add(omschrijvingenAR[c]);
			}
		}
		afdelingenAR = new String[afdelingenAL.size()];
		afdelingenAR = afdelingenAL.toArray(afdelingenAR);
				
		if(afdelingenAR.length==0) afdelingenAR = new String[]{NOTFOUND};
		afIDL = new JList<String>(afdelingenAR);
		if (d == DELETE || d == UPDATE) focus(afIDL);
		lAfID = new JScrollPane(afIDL);
		lAfID.setPreferredSize(DIM);
	}
	
	/*
	 * InsertUpdateDelete Machine
	 * 
	 * suid = 1 : Insert
	 * suid = 2 : Update
	 * suid = 3 : Delete
	 * suid = 4 : Search Machine Omschrijving
	 * suid = 5 : Search Machine Nummer
	 * 
	 */
	private void suidMachine(int suid) {
		JTextField mID = new JTextField();
		if (suid == INSERT) focus(mID);
		JTextField mOmschrijving = new JTextField();
		if (suid == SEARCHMO) focus(mOmschrijving);
		JList<String> mIDL = new JList<String>(database.getMachines());
		if (suid == SEARCHMID || suid == DELETE || suid == UPDATE) focus(mIDL);
		JScrollPane lMID = new JScrollPane(mIDL);
		lMID.setPreferredSize(DIM);
		guiAfdeling(INSERT);						// INSERT want ook lege afdelingen mogen getoond worden

		String titleStr = "";
		if(suid == SEARCHMID)	titleStr = "Zoeken op Machine-nummer";
		if(suid == SEARCHMO)		titleStr = "Zoeken op omschrijving Machine";
		if(suid == INSERT) 		titleStr = "Toevoegen van een nieuwe Machine";
		if(suid == UPDATE) 		titleStr = "Aanpassen van een bestaande Machine";
		if(suid == DELETE) 		titleStr = "Verwijderen van een Machine";
		if(suid == DELETE || suid == SEARCHMID) { 
			mOmschrijving.setEditable(false);
			afIDL.setEnabled(false);
		}
		/*
		 * JLabel met Listener om de omschrijving van de afdeling te tonen
		 */
		JLabel aO = new JLabel("");
		aO.setForeground(Color.BLUE);
		afIDL.addListSelectionListener(new ListSelectionListener(){
			@Override
			public void valueChanged(ListSelectionEvent arg0) {
				//System.out.println(ohNR.getSelectedValue());
				aO.setText(omschrijvingenAL.get(afIDL.getSelectedIndex()));
			}
		});
		afIDL.setSelectedIndex(0);	
		/*
		 * Afdeling van Machine updaten (enkel bij UPDATE & DELETE)
		 */
		if((suid == UPDATE) || (suid == DELETE || suid == SEARCHMID)) {
			mIDL.addListSelectionListener(new ListSelectionListener() {
				@Override
				public void valueChanged(ListSelectionEvent arg0) {
					mOmschrijving.setText(database.getMOmschrijvingen()[mIDL.getSelectedIndex()]);
					String[] afdelingen=database.getAfdelingen();
					String afdeling=database.getAfdeling(mIDL.getSelectedValue());
					for(int c=0;c<afdelingen.length;c++) {
						if(afdelingen[c].equals(afdeling)) afIDL.setSelectedIndex(c);
					}
				}
			});
			mIDL.setSelectedIndex(0);
		}
		
		int result=JOptionPane.CANCEL_OPTION;
		if (suid == SEARCHMO) {
			result = JOptionPane.showConfirmDialog(null, new Object[] {
					"Machine omschrijving" , mOmschrijving,
			}, titleStr ,JOptionPane.OK_CANCEL_OPTION);
		}
		if (suid == INSERT) {
			result = JOptionPane.showConfirmDialog(null, new Object[] {
					"Machine-nummer" , mID, 
					"Machine omschrijving" , mOmschrijving,
					"Afdeling ID" , lAfID,
					aO
			}, titleStr ,JOptionPane.OK_CANCEL_OPTION);
		}
		if ((suid == UPDATE) || (suid == DELETE || suid == SEARCHMID)) {
			result = JOptionPane.showConfirmDialog(null, new Object[] {
					"Machine-nummer" , lMID, 
					"Machine omschrijving" , mOmschrijving,
					"Afdeling ID" , lAfID,
					aO
			}, titleStr ,JOptionPane.OK_CANCEL_OPTION);
		}
		if (result == JOptionPane.OK_OPTION) {
			if (suid == UPDATE || suid == DELETE || suid == SEARCHMID) {
				if(mIDL.getSelectedValue().isEmpty()) return;
				if(mIDL.getSelectedValue().equals(NOTFOUND)) return;
			}
			if(suid == DELETE) database.deleteMachine(mIDL.getSelectedValue());					// wis afdeling
			if(suid == UPDATE) database.updateMachine(mIDL.getSelectedValue(), truncateString(mOmschrijving.getText(),LENMO), afIDL.getSelectedValue());
			if(suid == SEARCHMID) {
				data.setData(database.searchMachineNr(mIDL.getSelectedValue()));
				update();
				return;
			}
			if(suid == SEARCHMO) {
				data.setData(database.searchMachineOmschrijving(mOmschrijving.getText()));
				update();
				return;
			}
			if (suid == INSERT) {
				if(mID.getText().isEmpty()) return;
				String mIDStr = truncateString(mID.getText(), LENMID).toUpperCase();
				Boolean alreadyExists = false;
				String[] machines = database.getMachines();
				for(int c=0;c<machines.length;c++) {
					if(machines[c].toUpperCase().equals(mIDStr)) {
						alreadyExists = true;
					}
				}
				if (alreadyExists) {
					JOptionPane.showMessageDialog(this, "" 
							+ "<HTML><CENTER>De machine "
							+ mIDStr + " bestaat reeds.<BR><BR>"
							+ "Kan machine niet toevoegen."
							+ "</CENTER></HTML>");			
				} else {	
					database.addMachine(mIDStr, truncateString(mOmschrijving.getText(),LENMO), (String) afIDL.getSelectedValue());
				}
			}
			showAll();
		}
	}
	
	/*
	 * InsertUpdateDelete Onderdeel
	 * 
	 * suid = 1 : Insert
	 * suid = 2 : Update
	 * suid = 3 : Delete
	 * suid = 4 : Search Onderdeel Omschrijving
	 * suid = 5 : Search OH-nummer
	 */
	private void suidOnderdeel(int siud) {
		// Als we een onderdeel toevoegen, dan stellen we zelf een nieuw OH-nummer voor
		String newOhNR = "";
		if (siud == INSERT) {
			// 1. Haal alle ohNR's op
			String[] ohNRs = database.getOnderdelen();
			// 2. Haal het OH-gedeelte eraf en zoek naar het hoogste getal
			int nr=1;
			for(int c=0;c<ohNRs.length;c++) {
				String ohNR=ohNRs[c];
				//System.out.println(ohNR);
				ohNR=ohNR.substring(3, ohNR.length());
				int ohNRint;
				try {
					ohNRint = Integer.parseInt(ohNR);
				} catch (NumberFormatException e) {
					ohNRint = 0;
				}
				if ( ohNRint > nr) nr = ohNRint;
			}
			// nr is nu het hoogste bestaande OH-nr
			// 3. Verhoog dit nr met 1
			nr++;
			// 4. maak er terug een String van
			newOhNR = "OH-" + String.format("%010d", nr);
		}
		JTextField ohNR = new JTextField(newOhNR);
		JTextField oOmschrijving = new JTextField();
		if (siud == SEARCHOO || siud == INSERT) focus(oOmschrijving);
		JList<String> ohNRL = new JList<String>(database.getOnderdelen());    
		// Opm. onderdelen die in een machine zitten, kunnen ook verwijderd worden.
		// Bv. een onderdeel is niet meer nieuw verkrijgbaar en dus geschrapt uit het magazijn,
		// heeft dus geen OH-nummer meer. Het onderdeel zit dan nog in de machine, maar bestaat 
		// dan volgens de database niet meer. 
		if (siud == SEARCHOHNR || siud == DELETE || siud == UPDATE) focus(ohNRL);
		JScrollPane lOhNR = new JScrollPane(ohNRL);
		
		int result=JOptionPane.CANCEL_OPTION;
		String titleStr = "";
		if(siud == SEARCHOHNR) 	titleStr = "Zoek op OH-nummer";
		if(siud == SEARCHOO) 	titleStr = "Zoeken op Onderdeel omschrijving";
		if(siud == INSERT) 		titleStr = "Toevoegen van een nieuw Onderdeel";
		if(siud == UPDATE) 		titleStr = "Aanpassen van een bestaand Onderdeel";
		if(siud == DELETE) 		titleStr = "Wissen van een Onderdeel";
		if(siud == DELETE || siud == SEARCHOHNR ){
			oOmschrijving.setEditable(false);
		}
		if(siud == UPDATE || siud == DELETE || siud == SEARCHOHNR) {
			ohNRL.addListSelectionListener(new ListSelectionListener(){
				@Override
				public void valueChanged(ListSelectionEvent arg0) {
					if(ohNRL.getSelectedValue().equals(NOTFOUND)) return;
					oOmschrijving.setText(database.getOOmschrijvingen()[ohNRL.getSelectedIndex()]);
				}
			});
			ohNRL.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
			ohNRL.setSelectedIndex(0);	
			result = JOptionPane.showConfirmDialog(null, new Object[] {
					"OH-nummer" , lOhNR, 
					"Onderdeel omschrijving" , oOmschrijving
			}, titleStr ,JOptionPane.OK_CANCEL_OPTION);
		}
		if(siud == INSERT) {
			result=JOptionPane.CANCEL_OPTION;
			result = JOptionPane.showConfirmDialog(null, new Object[] {
					"OH-nummer" , ohNR, 
					"Onderdeel omschrijving" , oOmschrijving
			}, titleStr ,JOptionPane.OK_CANCEL_OPTION);
		}
		if(siud == SEARCHOO) {
			result=JOptionPane.CANCEL_OPTION;
			result = JOptionPane.showConfirmDialog(null, new Object[] {
					"Onderdeel omschrijving" , oOmschrijving
			}, titleStr ,JOptionPane.OK_CANCEL_OPTION);
		}
		if (result == JOptionPane.OK_OPTION) {
			if(siud == UPDATE || siud == DELETE) {
				if(ohNRL.getSelectedValue().isEmpty()) return;
				if(ohNRL.getSelectedValue().equals(NOTFOUND)) return;
			}
			if(siud == DELETE) database.deleteOnderdeel(ohNRL.getSelectedValue());					// wis onderdeel
			if(siud == UPDATE) database.updateOnderdeel(ohNRL.getSelectedValue(), truncateString(oOmschrijving.getText(),LENOO));
			if(siud == SEARCHOO) {
				if(oOmschrijving.getText().isEmpty()) return;
				data.setData(database.searchOnderdelenOmschrijving(truncateString(oOmschrijving.getText(),LENOO)));
				update();
				return;
			}
			if(siud == SEARCHOHNR) {
				data.setData(database.searchOnderdelenOHNR(ohNRL.getSelectedValue()));
				update();
				return;
			}
			if(siud == INSERT) {
				if(ohNR.getText().isEmpty()) return;
				String ohNRStr =  truncateString(ohNR.getText(), LENOID).toUpperCase();
				Boolean alreadyExists = false;
				String[] afdelingen = database.getAfdelingen();
				for(int c=0;c<afdelingen.length;c++) {
					if(afdelingen[c].toUpperCase().equals(ohNRStr)) {
						alreadyExists = true;
					}
				}
				if (alreadyExists) {
					JOptionPane.showMessageDialog(this, "" 
							+ "<HTML><CENTER>Het onderdeel "
							+ ohNRStr + " bestaat reeds.<BR><BR>"
							+ "Kan onderdeel niet toevoegen."
							+ "</CENTER></HTML>");			
				} else {	
					database.addOnderdeel(ohNRStr , truncateString(oOmschrijving.getText(),LENOO));
				}
			}
			showAll();
		}
	}
	
	/*
	 * InsertUpdateDelete Afdeling
	 * 
	 * iud = 1 : Insert
	 * iud = 2 : Update
	 * iud = 3 : Delete
	 */
	private void iudAfdeling(int iud) {
		JTextField afID = new JTextField();
		if (iud == INSERT) focus(afID);
		JTextField afOmschrijving = new JTextField();

		guiAfdeling(iud);							// Kiezen van afdeling (JList)
		int result=JOptionPane.CANCEL_OPTION;
		String titleStr = "";
		if(iud == INSERT) titleStr = "Toevoegen van een nieuwe Afdeling";
		if(iud == UPDATE) titleStr = "Aanpassen van een bestaande Afdeling";
		if(iud == DELETE) {
			titleStr = "Wissen van een lege afdeling";
			afOmschrijving.setEditable(false);
		}
		if(iud == UPDATE || iud == DELETE) {
			afIDL.addListSelectionListener(new ListSelectionListener(){
				@Override
				public void valueChanged(ListSelectionEvent arg0) {
					if(afIDL.getSelectedValue().equals(NOTFOUND)) return;
					afOmschrijving.setText(omschrijvingenAL.get(afIDL.getSelectedIndex()));
				}
			});
			afIDL.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
			afIDL.setSelectedIndex(0);	
			result = JOptionPane.showConfirmDialog(null, new Object[] {
					"Afdeling ID" , lAfID, 
					"Afdeling omschrijving" , afOmschrijving
			}, titleStr ,JOptionPane.OK_CANCEL_OPTION);
		}
		if(iud == INSERT) {
			result = JOptionPane.showConfirmDialog(null, new Object[] {
					"Afdeling ID" , afID, 
					"Afdeling omschrijving" , afOmschrijving
			}, titleStr ,JOptionPane.OK_CANCEL_OPTION);
		}
		if (result == JOptionPane.OK_OPTION) {
			if(iud == UPDATE || iud == DELETE) {
				if(afIDL.getSelectedValue().isEmpty()) return;
				if(afIDL.getSelectedValue().equals(NOTFOUND)) return;
			}
			if(iud == DELETE) database.deleteAfdeling(afIDL.getSelectedValue());					// wis afdeling
			if(iud == UPDATE) database.updateAfdeling(afIDL.getSelectedValue(), truncateString(afOmschrijving.getText(),LENAFO));
			if(iud == INSERT) {
				if(afID.getText().isEmpty()) return;
				String afIDStr =  truncateString(afID.getText(), LENAFID).toUpperCase();
				Boolean alreadyExists = false;
				String[] afdelingen = database.getAfdelingen();
				for(int c=0;c<afdelingen.length;c++) {
					if(afdelingen[c].toUpperCase().equals(afIDStr)) {
						alreadyExists = true;
					}
				}
				if (alreadyExists) {
					JOptionPane.showMessageDialog(this, "" 
							+ "<HTML><CENTER>De afdeling "
							+ afIDStr + " bestaat reeds.<BR><BR>"
							+ "Kan afdeling niet toevoegen."
							+ "</CENTER></HTML>");			
				} else {	
					database.addAfdeling(afIDStr , truncateString(afOmschrijving.getText(),LENAFO));
				}
			}
			showAll();
		}
	}
		
	/*
	 * Zorg JComponent de focus krijgt.
	 * 
	 * Zie <https://stackoverflow.com/questions/6251665/setting-component-focus-in-joptionpane-showoptiondialog>
	 * 
	 * Opmerking: 
	 *    Onder MS\Windows 10 is het voldoende om t.requestFocusInWindow te doen bij ancestorAdded.
	 *    Echter onder Debian GNU/Linux Stretch <https://www.debian.org/> werkt dit niet[1].
	 *    --> extra code nodig via addFocusListener, maar omdat dit onder Windows 10 dan weer 
	 *        problemen geeft (2 keer moeten klikken alvorens van veld te wisselen) wordt er getest
	 *        onder welk OS dit programma wordt uitgevoerd via System.getProperty("os.name").equals("Linux")).
	 *        
	 *    Niet getest met andere OSsen.		
	 *    
	 *    [1] onder Debian GNU/Linux getest met: 
	 *    - KDE <https://www.kde.org/> Desktop Environment
	 *    - dwm <http://dwm.suckless.org/>  WindowManager 
	 *    
	 */
	private void focus(JComponent c) {
		c.addAncestorListener( new AncestorListener()
		{
			@Override
			public void ancestorAdded(AncestorEvent event) {
				c.requestFocusInWindow();
			}
			@Override
			public void ancestorMoved(AncestorEvent event) {
			}
			@Override
			public void ancestorRemoved(AncestorEvent event) {
			}
		} );
		
		if (System.getProperty("os.name").equals("Linux")) {
			c.addFocusListener( new FocusListener()
			{
				boolean isFirstTime = true;
				@Override
				public void focusGained(FocusEvent arg0) {
				}
				@Override
				public void focusLost(FocusEvent arg0) {
					if( isFirstTime )
					{
						// When we lose focus, ask for it back but only once
						c.requestFocusInWindow();
						isFirstTime = false;
					}
				}

			} );
		}
	}

	/*
	 * Beperk de lengte van een string tot 'max'
	 */
	private String truncateString(String str, int max) {
		return str.substring(0, Math.min(str.length(), max));
	}
		
	/*
	 * Toon alles wat in de database zit
	 */
	public void showAll() {
		data.setData(database.getAll());
		update();
	}
		
	/*
	 * Update het scherm met de nieuwe informatie
	 */
	public void update() {
		model.setDataVector(data.getData(), data.getNames());
		MyCellRenderer myCellRenderer = new MyCellRenderer(data);
		for(int c=0;c<data.getNames().length;c++) {
			table.getColumnModel().getColumn(c).setCellRenderer(myCellRenderer);
		}
	}
}
