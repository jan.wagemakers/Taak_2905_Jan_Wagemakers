/*
 * Deze klasse dient om de JTabel van de nodige kleurtjes te voorzien
 */
package be.janwagemakers;
import java.awt.Color;
import java.awt.Component;

import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

public class MyCellRenderer extends DefaultTableCellRenderer {
	private static final long serialVersionUID = 1L;
	private Data data;

	@Override
	public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column)
    {
        Component cellComponent = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
        
        String current = table.getValueAt(row, column).toString();		// De waarde van de huidige cel
        boolean[] colors = data.getColors();							// lijst false/true per row
        Color color;
        if (colors[row]) {
        	color = Color.WHITE;										// true = Color.WHITE
        	if(current.isEmpty()) color = Color.LIGHT_GRAY;				// lege cell --> donker maken
        } else {
        	color = Color.YELLOW;										// false = Color.YELLOW
        	if(current.isEmpty()) color = Color.ORANGE;					// lege cell --> donker maken
        }
        cellComponent.setBackground(color);								// zet de achtergrond van de cell
        return cellComponent;
    }

	public MyCellRenderer(Data data) {
		this.data = data;												// kleurtjes hangen af van de gegevens in 'Data data'
	}

}
