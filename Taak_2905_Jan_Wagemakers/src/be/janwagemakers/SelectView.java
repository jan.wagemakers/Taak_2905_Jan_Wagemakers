/*
 * Deze klasse is verantwoordelijk voor het bijhouden van welke kolommen er al dan niet zichtbaar moeten zijn.
 */

package be.janwagemakers;

public class SelectView {
	// De namen van de verschillende kolommen
	private String names[] = {"OH-nummer", "Omschrijving Onderdeel", "Machine-nummer", "Omschrijving Machine", "Afdeling", "Omschrijving Afdeling"};
	// De start situatie (true = zichtbaar)
	private boolean[] view = {true, true, true, true, true, true};
	private GUI gui;
	
	public SelectView(GUI gui) {
		this.gui = gui;
	}

	/*
	 * is kolom[f] zichtbaar?
	 */
	public boolean getView(int f) {
		return view[f];
	}

	/*
	 * zet zichtbaarheid van kolom[f]
	 */
	public void setView(int f, boolean v) {
		view[f] = v;
		gui.update();
	}
	
	/*
	 * toggle de zichtbaarheid van kolom[f]
	 */
	private void toggleView(int f) {
		setView(f, !getView(f));
	}
	
	/*
	 * Return : de namen van de verschillende kolommen
	 */
	public String[] getNames() {
		return names;
	}
	
	/*
	 * Return : hoeveel kollommen zijn zichtbaar
	 */
	public int countView() {
		int countTrue = 0;
		for (int c=0; c<view.length; c++) {
			if (view[c]==true) countTrue++;
		}
		return countTrue;
	}
	
	/*
	 * toggle de zichtbaar op basis van de kolom-naam
	 */
	public void toggleView(String name) {
		for(int c=0;c<names.length;c++) {
			if(names[c].equals(name)) toggleView(c);
		}
	}
}
