/*
 * In deze klasse worden de gegevens die op het scherm getoond worden bewaard.
 * Omdat steeds alle velden raadpleegbaar zijn, zijn er geen aparte klassen/objecten gemaakt
 * voor Afdeling, Machine, Onderdeel
 *  
 */

package be.janwagemakers;

import java.util.ArrayList;

public class Data {
	private SelectView selectView;			// Keuze van welke velden er zichtbaar zijn
	private String[][] information;			// Alle informatie inclusief de niet zichtbare velden
	private String[][] selectedInfo;		// De informatie die zichtbaar is
	// Onderstaande wordt gebruikt om getData te versnellen
	private String[][] oldInformation;		// Oude informatie
	private int oldCountView;				// Oude countView
	
	public Data(SelectView selectView) {
		this.selectView = selectView;
	}
	
	/*
	 * Ophalen van de Titlebalk voor de JTable (GUI)
	 */
	public String[] getNames() {
		// We maken een omweg via een String[][] zodat we getData() kunnen hergebruiken.
		String[][] in  = new String[1][selectView.getNames().length];
		String[][] out = new String[1][selectView.countView()];
		in[0] = selectView.getNames();		// De namen van de velden komen van selectView
		out = getData(in);
		return out[0];
	}
	
	/*
	 * De informatie die via de JTable getoond wordt zit in information
	 */
	public void setData(String[][] information) {
		this.information = information;
	}
	
	/*
	 * Logischer wijze hebben we een getter nodig om aan deze informatie te geraken
	 */
	public String[][] getData() {
		return getData(information);
	}
	
	/*
	 * Geeft een boolean[] array terug, gebruikt voor de wisseling van kleurtjes in de JTable (GUI)
	 */
	public boolean[] getColors() {
		// System.out.println("getColors wordt aangeroepen");
		Boolean color = false;								// false --> kleur 1
		String[][] selectedInfo = getData();				// Haal de huidige information op
		boolean[] colors = new boolean[information.length];	// Nieuwe array met dezelfde lengte als information	
		
		String previous = "?";								// Vorige is onbekend
		for (int c=0;c<selectedInfo.length;c++) {			// Ga heel de array af
			String current = selectedInfo[c][0];			// Als vorige != huidige
			if(!previous.equals(current))color = !color;	// ( ----|>o----   Not ) = wissel van kleurtje
			colors[c] = color;								// bewaar kleurtje
			previous = current;
		}
	    return colors;
	}
	
	/*
	 * In information zitten alle velden.
	 * 
	 * Maar via selectView kan de gebruiker er voor kiezen niet alle velden te bekijken.
	 * Via deze methode worden enkel die velden teruggegeven die de gebruiker ook echt wil zien.
	 * 
	 */
	private String[][] getData(String[][] in) {
		// Versnellen van getData : als er niets is veranderd, moeten we deze berekening niet opnieuw uitvoeren ///////////////
		if(selectView.countView() == oldCountView && in == oldInformation) return selectedInfo;
		oldInformation = in;
		oldCountView = selectView.countView();
		//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			
		if(selectView.countView()==0) return new String[][]{{"Zichtbaarheid = 0"}};		// Gebruiker wil niets zien, 
		 																				// het is niet de bedoeling om dit toe te laten.
		// Maak een nieuwe String[][] array met de juiste lengte aan
		selectedInfo = new String[in.length][selectView.countView()];
		
		// Vul deze met de nodige velden
		for(int x=0;x<in.length;x++) {
			int r=0;
			for(int y = 0;y<selectView.getNames().length;y++) {
				if(selectView.getView(y)) {
					selectedInfo[x][r] = in[x][y];
					if(selectedInfo[x][r] == null) selectedInfo[x][r] = "";
					r++;
				}
			}
		}
		
		/*
		 * Neem de volgende situatie:
		 * 
		 * OH-nummer       |  Omschr.    |  MID   | M.Omschrijving       |  AFID |  Afd.Omschrijving
		 * ----------------+-------------+--------+----------------------+-------+-------------------------
		 * OH-000000000001 |  regelaar	 | T014	  | Elek. CarboneerOven	 | Oven	 | Thermische Behandeling
		 * OH-000000000001 |  regelaar	 | T017	  | Elek. CarboneerOven	 | Oven	 | Thermische Behandeling
		 * 
		 * Stel dat de gebruiker nu enkel OH-nummer en Omschrijving onderdeel wil zien. 
		 * Dan krijg je twee rijen die er juist het zelfde uit zien
		 * 
		 * Daarom gaan we deze dubbele rijen verwijderen. 
		 */
		ArrayList<String[]> temp = new ArrayList<>();					// Maak een nieuwe ArrayList

		for(int x=0;x<selectedInfo.length;x++) {						// loop door de gehele array
			String xx = "";
			for(int c=0;c<selectView.countView();c++) {
				xx = xx + selectedInfo[x][c];							// Maak van alle velden in de rij een lange String
			}
			
			boolean same=false;
			for(int y=0;y<temp.size();y++) {							// Is deze rij reeds aanwezig in de ArrayList?
				String yy = "";	
				for(int c=0;c<selectView.countView();c++) {
					yy = yy + temp.get(y)[c];
				}
				if(xx.equals(yy)) same=true;
			}
			if(!same && !xx.isEmpty())  temp.add(selectedInfo[x]);		// Nee, voeg dan toe aan de ArrayList (leeg wordt ook niet toegevoegd).
		}
		
		// converteer deze ArrayList terug naar een 2D String[][] array
		selectedInfo = new String[temp.size()][selectView.countView()];
		selectedInfo = temp.toArray(selectedInfo);
		return selectedInfo;
	}
}
