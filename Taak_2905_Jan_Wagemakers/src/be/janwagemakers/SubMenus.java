/*
 * Klasse om het toevoegen van submenu's te vereenvoudigen
 */
package be.janwagemakers;

import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.KeyStroke;

public class SubMenus {
	
	/*
	 * Standaard submenu's aanmaken
	 */
	public SubMenus(GUI gui, JMenu menu, String[] names) {
		new SubMenus(gui, menu, names, false);
	}
	
	/*
	 * Submenu's aanmaken. 'boolean stay' bepaald het type submenu (true = StayOpenCheckBox)
	 */
	public SubMenus(GUI gui, JMenu menu, String[] names, boolean stay) {
		ActionListener actionListener = new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent event) {
				gui.menu((JMenuItem) event.getSource());		// terugmelding aan gui dat er op een menuItem is geklikt.
			}};
		
		for(int i=0;i<names.length;i++) {
			JMenuItem item;
			String n[] = names[i].split("_");					// _ wordt gebruikt om ACCEL keys te definieren
			if (!stay) {
				item=new JMenuItem(n[0]);
			} else {
				item=new StayOpenCheckBoxMenuItem(n[0]);
			}
			if (n.length >1) {
				KeyStroke f = KeyStroke.getKeyStroke(n[1].charAt(0), Toolkit.getDefaultToolkit ().getMenuShortcutKeyMask());
				item.setAccelerator(f);
			}
							
			menu.add(item);
			item.addActionListener(actionListener);
		}
	}
}
